#!/bin/bash

./configure --prefix=${PREFIX} \
            --host=${HOST}     \
            --build=${BUILD}   \
            --disable-udev
make
make install
