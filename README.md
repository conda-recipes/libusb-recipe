# libusb conda recipe

Home: https://libusb.info

Package license: LGPL-2.1

Recipe license: BSD 3-Clause

Summary: A cross-platform library to access USB devices
